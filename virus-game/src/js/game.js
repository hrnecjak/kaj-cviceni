const State = {
    READY: "Ready",
    RUNNING: "Running",
    OVER: "Over"
};

class Game {

    constructor(){
        this.score = 0;
        this.fails = 0;
        this.state = State.READY;

        console.log("Game ready")
    }
    run() {

        console.log("Game starting");
        this.state = State.RUNNING;
        console.log("Loop entered");
        setInterval(()=>this.createVirus(), 500);
        //this.state = State.OVER
    }

    createVirus() {
        let scoreCallback = function(){
            this.score = this.score+1;
            document.getElementById("gameUI").innerText ="Score:" + this.score;
        }.bind(this);
        console.log("Creating virus");
        let virus = new Virus(scoreCallback);
    }



}

class Virus {
    constructor(scoreCallback){
        let maxX = parseInt(window.getComputedStyle(document.getElementsByClassName("worldMapImg")[0]).width);
        let maxY = parseInt(window.getComputedStyle(document.getElementsByClassName("worldMapImg")[0]).height);

        console.log("maxX: " + maxX + " maxY: " + maxY);

        let x = Math.floor((Math.random()) * maxX);
        //x = x < 20 ? 20 : x > maxX - 20 ? maxX - 20 : x;

        let y = Math.floor((Math.random()) * maxY);
        //y = y < 20 ? 20 : y > maxY - 20 ? maxY - 20 : y;

        let virusNode = document.createElement("img");
        virusNode.setAttribute("src", "../resources/virus.png");
        virusNode.classList.add("virus");
        virusNode.style.left = x+"px";
        virusNode.style.top = y+"px";
        console.log("X: " + x + " Y: " + y);
        virusNode.addEventListener("click", evt => this.destroy(scoreCallback));
        document.body.appendChild(virusNode);

        this.virusNode = virusNode;
    }

    destroy(callback){
        document.body.removeChild(this.virusNode);
        callback();
    }
}

let game = new Game();
game.run();




